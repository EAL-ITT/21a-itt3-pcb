---
title: '21A ITT3 PCB'
subtitle: 'Circuit catalogue'
filename: 'circuit_catalogue'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-06-15
email: 'nisi@ucl.dk'
left-header: \today
right-header: Circuit catalogue
skip-toc: false
---

# Circuit catalogue

Below you will find ideas for circuits that you can design a PCB from.  

More ideas can be found at digi key's reference design library [https://www.digikey.com/reference-designs/en](https://www.digikey.com/reference-designs/en)

# RP2040 IoT development board

The RP2040 is the new microcontroller developed by Raspberry Pi Foundation. 
It is the basis of the RPi Pico development board and numerous other development boards.

For this circuit you need to build a IoT development board. 
The RP2040 is well documented if you want to build a PCB with it, the Raspberry Pi foundation has written a hardware design guide  
[https://datasheets.raspberrypi.org/rp2040/hardware-design-with-rp2040.pdf](https://datasheets.raspberrypi.org/rp2040/hardware-design-with-rp2040.pdf)  
as well as a great datasheet  
[https://datasheets.raspberrypi.org/rp2040/rp2040-datasheet.pdf](https://datasheets.raspberrypi.org/rp2040/rp2040-datasheet.pdf)

The main requirements when building this circuit into a PCB is to have: 
* Use SMT components for everything possible
* GPIO's available as pins
* Power supply section (Buck Converter)
* Crystal oscillator
* Flash memory
* USB connector 
* One or more sensors onboard (accelerometer, IMU, temperature sensor etc.)
* Other functionality that you decide

Secondary requirements is to put some sort of wireless communication chip onboard, wifi, Bluetooth, Lora or other.
If you do not manage to add wireless connectivity onboard you should make one or more serial busses available on the board (SPI, I2C, UART)

Phil's Lab has done a video where he explains a similar design and layout done in Altium Designer (EDA software like OrCad) that you can use for inspiration [https://youtu.be/X00Cm5LMNQk](https://youtu.be/X00Cm5LMNQk)

**Level: Medium**

# atmega328 IoT development board

A project very similar to the RP2040 IoT development board but instead of the RP2040 it uses an atmega328 microcontroller.
The atmega328 is the microcontroller used in the famous Arduino Uno development board.

The Arduino Uno is open hardware and this article gives a good walkthrough of the UNO R3 hardware [https://www.allaboutcircuits.com/technical-articles/understanding-arduino-uno-hardware-design/](https://www.allaboutcircuits.com/technical-articles/understanding-arduino-uno-hardware-design/) 

The UNO reference design is also available [https://www.arduino.cc/en/uploads/Main/arduino-uno-schematic.pdf](https://www.arduino.cc/en/uploads/Main/arduino-uno-schematic.pdf)

The main requirements when building this circuit into a PCB is to have: 
* Use SMT components for everything possible
* GPIO's available as pins
* Power supply section (Buck Converter)
* Crystal oscillator
* USB bridge 
* One or more sensors onboard (accelerometer, IMU, temperature sensor etc.)
* Other functionality that you decide

Secondary requirements is to put some sort of wireless communication chip onboard, wifi, Bluetooth, Lora or other.
If you do not manage to add wireless connectivity onboard you should make one or more serial busses available on the board (SPI, I2C, UART)

**Level: Hard**

# RPi IoT hat

This circuit project is about building an add-on PCB (hat) to the Raspberry Pi.
The hat needs to follow the official har standard outlined by the Raspberry Pi foundation described here [https://github.com/raspberrypi/hats](https://github.com/raspberrypi/hats)
Make sure to also read and understand the design guide [https://github.com/raspberrypi/hats/blob/master/designguide.md](https://github.com/raspberrypi/hats/blob/master/designguide.md)

The main requirements when building this circuit into a PCB is to have: 
* Use SMT components for everything possible
* RPi Hat according to official hat standard
* Several sensors onboard (accelerometer, IMU, temperature sensor etc.)
* Other functionality that you decide

**Level: Easy**

# RPi Pico IoT carrier board

This circuit project is about building an add-on PCB (carrier board) to the Raspberry Pi Pico.
The RPi Pico is very new and i struggled to find a proper design guide or hat standards.

The RPi Pico board is constructed as a "component" that you can solder to your own board (carrier board) and this is exactly what this project is about.
Phil's Lab has made a video explaining how he design and build a RPi Pico carrier board [https://youtu.be/efPsvz6j6ao](https://youtu.be/efPsvz6j6ao)

The main requirements when building this circuit into a PCB is to have: 
* Use SMT components for everything possible
* RPi pico carrier board
* Several sensors on the carrier board (accelerometer, IMU, temperature sensor etc.)
* Other functionality that you decide

**Level: Medium**

# RPi Universal robots tool interface hat

This circuit project is about building an add-on PCB (hat) to the Raspberry Pi.
The hat needs to follow the official har standard outlined by the Raspberry Pi foundation described here [https://github.com/raspberrypi/hats](https://github.com/raspberrypi/hats)
Make sure to also read and understand the design guide [https://github.com/raspberrypi/hats/blob/master/designguide.md](https://github.com/raspberrypi/hats/blob/master/designguide.md)

The main purpose is to have a hat that is an interface between the RPi and the universal robots tool interface.
This is mainly a matter of level conversion between the RPi 3.3V and the Universal robots 12V.
A similar project for the Arduino has been done in a previous PCB elective course, the Arduino uses 5V and is therefore not directly compatible with the RPi, you can use the project as inspiration but change the 5V part to 3.3V [https://gitlab.com/20a-itt3-pcb-students-group/itt3-pcb-gytis](https://gitlab.com/20a-itt3-pcb-students-group/itt3-pcb-gytis)

This PCB is in high demand from the people using the UCL automation lab and you should work together with them to specify and test the board.  

The main requirements when building this circuit into a PCB is to have: 
* Use SMT components for everything possible
* RPi Hat according to official hat standard
* interface between the RPi and the universal robots tool interface
* Other functionality that you decide

**Level: Easy**