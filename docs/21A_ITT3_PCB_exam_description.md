---
title: '21A ITT3 PCB exam description'
subtitle: 'Exam description'
filename: '21A_ITT3_PCB_exam_description'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-11-14
email: 'nisi@ucl.dk'
left-header: \today
right-header: Exam description
skip-toc: false
semester: 21A
---


# Document content

This document describes the exam hand-in requirements for the PCB elective course.

# Exam description

The exam is individual.  

The conclusion of the PCB elective course is a written report where students present the process of designing their
PCB, presentation of the physical product (Populated PCB) and reflections on how the learning goals of the elective course have been met by the student.

# Grading

Grading is based on the report.  
Each student is graded according to the fullfillment of the PCB course learning goals described in the [course material](https://eal-itt.gitlab.io/21a-itt3-pcb/21A_ITT3_PCB_weekly_plans.pdf) and [lecture plan](https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/21A_ITT3_PCB_lecture_plan.html)  

Grading is 7 grade scale.  
The exam is with internal sensor. 

#  Report and hand-in requirements

The hand-in is individual.  

Students are expected to hand-in a report on wiseflow.   

Page count: Minimum 5 and maximum 15 standard pages.  

*The title page, list of contents, bibliography and annexes do not count towards the required number of pages. Annexes are not assessed.*  
*A standard page means 2,400/x characters including spaces and footnotes.*  

**Minimum content requirements:**

* Front page with an image of finished PCB, report title, author name, semester, course, school logo, date and character count
* A description of the PCB production process from idea to finished product (students should use the project worklog and other notes produced during the course)
* Simulation, assembly and test results
* Reflection on how the student have worked with the individual learning goals of the PCB elective course and the students learning outcome
* A desription of how the students PCB gitlab project is organized, ie. where to find information 
* A link to the students PCB gitlab project 
* Appendix needs to be included as a seperate section at the end of the report document, not as additional material

**Report guidelines:**

* Do not include images without purpose, that is, an image should only be included to compliment text.
* All images should have a caption number and a short description in the caption.
* Text in images should be the same size as report text.
* Page numbering (page 1) starts on the first page after table of contents 
* Chapters should be numbered
* Citation and use of other people's work should be referrenced. Use of a reference standard is encouraged (Harvard, PAP, IEEE etc.)  
If you do not reference other peoples work it is considered plagiarism which can have serious consequences, read more here [https://www.ucl.dk/international/for-students/cheating-at-the-exam-and-plagiarism](https://www.ucl.dk/international/for-students/cheating-at-the-exam-and-plagiarism)

# Hand-in dates

* 1st attempt - 08-12-2021 kl. 18:00 
* 2nd attempt - 07-01-2022 kl. 18:00
* 3rd attempt - 19-01-2022 kl. 18:00

# Additional information