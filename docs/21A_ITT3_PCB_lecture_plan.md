---
title: '21A ITT3 PCB'
subtitle: 'Lecture plan'
filename: '21A_ITT3_PCB_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-06-15
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
semester: 21A
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait20, (3rd semester)
* Name of lecturer and date of filling in form: NISI 2021-06-15
* Title of the course, module or project and ECTS: Elective subject (PCB Production), 5 ECTS
* Required readings, literature or technical instructions and other background material: Announced in weekly plans and exercises

See the [course website](https://eal-itt.gitlab.io/21a-itt3-pcb) for detailed daily plan, links, references, exercises and so on.

| Teacher | Week  | Content |
|:---- |:-----: |:----- |
| NISI, Introduction, the PCB process cycle, circuit research, circuit decision | 34    | In this week we will begin by getting an overview about the PCB design and manufacturing process. You will work with researching and selecting the circuit for your PCB project. You also need to setup a gitlab project to manage your different orcad projects and documentation. The gitlab project is also used to manage your own work. A working Orcad installation including a student license is required for the PCB elective course, if you haven't already installed it on your computer you will also have to do it this week. | 
| NISI, Nordcad beginner course, Schematic, simulation | 35    | To get familiar with the Orcad suite of programs you will have to complete nordcads beginner course, this involves both schematic, simulation and PCB layout exercises. Theory this week will focus on Orcad capture and PCB editor | 
| NISI, SMT Component packages, footprints, component research | 36    | Surface mount technology is more or less the standard in PCB design these days. The benefits are many. SMT components comes in different sizes and packages that you need to familiarize yourself with before you can work with them. You will have to research which components you need, find datasheet and find footprints for all of the components in your schematic as well as assign them in orcad capture. |
| NISI, Design rule check, netlist | 37    | Before being able to export your schematic from Capture to PCB editor you need to ensure that you have no errors in Capture. This is done with a design rule check. You also need to build a netlist before exporting to PCB editor. The final exercises will teach you how to do a board outline as well as designing mechanical symbols to create mounting holes on your board |
| NISI, Nordcad workshop | 38    | This week we have a specialist from Nordcad in Aalborg. He will be giving a workshop in the Orcad suite of programs to get you familiar with some of the more advanced functionality. If you have specific questions this is the time to ask them. |
| NISI, Design for manufacturing workshop | 39    | This week we have a special guest, he is the person in charge of the design for manucfacturing at Universal Robots. He has been doing design for manufacturing most of his life and is an expert in the subject. He will be giving you a unique insight into the PCB manufacturing process as well as things to consider before sending your PCB to manufacturing |
| NISI, Factory design constraints, Board layout, Layers | 44    | PCB manufacturers has different machines and different constraints that you need to consider in order for them being able to manufacture your PCB. This week is all about getting you design ready for manufacturing |
| NISI, Routing, gerber and drill files, ordering PCB's | 45    | What kind of files do you have to send of to the PCB manufacturer? This week we will look at how you can prepare the different manufacturing files and how you can place an order |
| NISI, Documentation (BOM, schematics, other design files) | 46    | Documentation of PCBs are essential for several reasons. It is practically impossible to assemble a PCB without documentation, but what types of documentation is needed and how do you create it ? |
| NISI, PCB Assembly, PCB testing | 47    | Hopefully you have received your produced PCB and are now ready to assemble it with the components needed. We will look at the requirements of the exam report that you need to hand-in week 49 |
| NISI, PCB rev2 in Kicad | 48    | Orcad is a very capable suite of programs and a industry standard, but it is also very expensive to purchase. If you work in a company without expensive Orcad licenses there are other free options that can be used. One of these options is Kicad. We will focus on how you can transfer your design to Kicad and implement possible changes for V2 of your PCB |
| NISI, Report, exam hand-in | 49    | Course exam report hand-in is near, make sure to work on your report |


# General info about the course, module or project

The purpose of the course is to learn about the PCB design and manufacturing proces.  
During the course the student will design and manufacture at least one PCB, preferrable two. 
Students will decide on the circuit they want to design, manufacture and test.

## The student’s learning outcome

The subject area includes fundamentals of PCB design and manufacturing, the use of EDA software as well as design, development, testing and documentation of PCB's.
At the end of the course, the student will be able to analyse circuits, design and manufacture PCB's for embedded systems using both the OrCad suite and KiCad. 

The below learning goals are high level learning goals that is translated into differentiated (3 levels) learning goals in the weekly plans.  
The purpose of translating and differentiating the learning goals is for students to actively use learning goals as a tool to evaluate themselves, during the course and to plan for recap before the exam.

### Knowledge

* (LGK1) Project management of IT development projects
* (LGK2) Quality and resource management as part of a development project and as part of the 
management of the maintenance of IT operations
* (LGK3) Electronic modules in overview, and how selected modules are constructed
* (LGK4) Technical mathematics applied to the subject area as needed to understand electronics 
and/or communications
* (LGK5) Signal handling in a general sense, and how it is used and incorporated in solutions

### Skills

* (LGS1) Communicate orally and in writing with both professionals and customers
* (LGS2) Select, adapt and apply embedded systems and components in secure, sustainable solutions
* (LGS3) Construct and use test systems
* (LGS4) Document and communicate tasks and solutions using embedded components and systems

### Competences

* (LGC1) Undertake customer assignments to translate customer requirements into secure solutions
* (LGC2) Undertake planning, manage the student’s own technical tasks and take part in projects
* (LGC3) Undertake analysis, needs analysis, design, development and testing of secure embedded and sustainable solutions
* (LGC4) Undertake analysis, diagnosis, testing and maintenance of the technology involved in work with electronic systems, taking account of financial, environmental and quality requirements
* (LGC5) Acquire new knowledge, skills and competencies in the subject area

## Method

The course focuses on practice mixed with applied theory.  
Theory lectures will take place mostly in the afternoon and students will work independently mostly in the morning.  
Practice is very important when designing PCB's and the proces is time-consuming. It is therefore crucial that students spend all working hours to gain the neccesary knowledge, skills and competences from the course.   
To facilitate different learning styles video material supporting the content will be made available when possible.  

### PBL – Problem Based Learning 

A key design factor in the IT Technology programme is the principle that students 
develop better and more relevant competencies by tackling practical problems as 
opposed to working exclusively with theoretical textbooks.  

As a didactic concept, PBL enables the building and maintenance of motivation 
along with the simultaneous development of general and personal competencies.  

PBL is planned on the basis of three key principles: 
* Problem based activity
* Participant engagement in governance
* Situational realism in a learning environment that 
recognises accomplishment and a high level of professional competence (Stegeager, 
Nicolaj, Overgaard Thomassen, Anja, Stentoft, Diana, Egelund Holgaard, Jette et al. 
2020).  

Theory based instruction uses dialogues during presentations and subsequent exercises.   

Various types of instruction and work are alternated, thus connecting the students’ own 
practical experience with theoretical analyses and perspectives.   

New and classic theories and methods are presented for discussion in light of the students’ own practical experience.  

The professional domain is examined in light of studies, development work, 
and new knowledge and research in the area.

### Flipped Learning

Flipped Learning is a pedagogical method that moves instructional learning from a shared environment to an individual one.  

The shared learning environment is transformed into a dynamic and interactive learning environment in which the instructor guides students in the process of learning, understanding, and applying a number of concepts and methods within a given professional subject area. Teaching is one on one, often using online 
resources.  

As a result, class time is freed up so that the instructor may provide, face to 
face with the students, a better and more individual guidance – with the aim of improving the students’ learning experience.  

One of the greatest benefits of flipped learning is the opportunity to customise and individualise the instruction for each student.  

Unfortunately, one of the greatest weaknesses of the method is that success depends on the degree to which students have done the preparatory homework before attending class (Bergmann, 
Jonathan & Sams, Aron 2015: 20-21).  

The schedule in the IT Technology programme includes time slots set aside for the students to prepare for class individually.

### Practical Lab Instruction

Lab work gives the students an opportunity to learn certain skills that are difficult to learn through other means of instruction.  

The IT Technology programme offers 
students an electronics lab and a network lab.  

Lab work supports practical skills like handling equipment and materials, building 
systems, observing and collecting data, analysing and interpreting data as well as skills in collaboration and documentation.  

While such learning opportunities are made available, learning does not automatically occur.  
The instructor must help students learn.  

Lab learning also incorporates the students’ thoughts and observations with respect to the experimental work.  

Troubleshooting by students is required when a practical exercise does not evolve as expected or does not show the anticipated results.  

Lab instruction may, as an example, include:
* A preparatory lecture in which theory is connected to the assignment at hand
* A detailed step-by-step manual for the students to follow
* A final report to be submitted by the students. 

Frequently, the instructor, and perhaps the students as well, are aware of the results of 
an experiment in advance. This type of instruction leaves students with little or no influence on what they are to investigate and how.  

There are benefits to the instructor giving more influence to the students. It increases their learning because more influence requires them to consider purposes, hypotheses, and the design of the experiment and relate these to the scientific concepts.  
In other words, the instructional program may be structured to have the students apply and reflect on professional concepts and knowledge prior to writing a report on the experimental work.  

By giving learning activites a context prior to and during the lab work, instructors give students the opportunity to think about and discuss, in depth, concepts and challenges 
with classmates and the instructor.

## Equipment

Windows computer with OrCad and Nordcad student license as well as KiCad installed.

## Projects with external collaborators  

Nordcad workshop (Martin Lange Nonboe, Nordcad)  
Design for manufacturing (Søren Knudsen, Universal Robots)

## Test form/assessment

For more information see the semester description on [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology) for details on obligatory learning activities and exams.

## Study activity model

The course activities are divided in 4 categories named K1, K2, K3 and K4.  

The categories are defined as part of the Study Activity Model and is a tool for students to see the workload expected (135 efficient working hours) as well as expectations and responisibility for each category.  

Explanation of each category are in the image below as well as the distribution of activities in both hours and percentage.

In the [weekly plans](https://eal-itt.gitlab.io/21a-itt3-pcb/weekly-plans) students can get an overview of how the categories are distributed in lectures.

![study activity model](21a_itt3_pcb_study_activity_model.png)

## Other general information

None at this time.
