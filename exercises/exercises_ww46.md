---
Week: 46
tags:
- Documentation
- Overview
---


# Exercises for ww46

## Exercise 1 - Course exercises overview

### Information

To make sure that you did all the exercises, including exercises from this week you need to make a todo list in your gitlab project `readme.md` file.
This is a nice way to get an overview of the exercises you did not complete yet.  

Completion of the exercises will also help you in writing the report at the end of the course, take a look at the exam description to see the report requirements

### Exercise instructions

1. In your gitlab projects `readme.md` Make a todo list of all the exercises from the PCB elective course.  
2. For each exercise in the list, check to see that you did all deliverables (documentation, files etc.) and put a checkmark on that exercise
3. Make a written plan on how you are going to catch up on exercises if you did not do all the exercises yet.

Suggested TODO list layout:  

``` 
- [ ] Week 34
    - [ ] Exercise 1 - Circuit research
    - [ ] Exercise 2 - Setup gitlab documentation project
    - [ ] Exercise 3 - Project log
    - [ ] Exercise 4 - OrCad installation and license
- [ ] Week xx
    - [ ] Exercise 1 - xxxxxxx
    - [ ] Exercise 1 - xxxxxxx
```

The `- [ ]` syntax will turn into a clickable checkbox in the gitlab `readme.md` file

## Exercise 2 - Documentation

### Information

Documentation on your PCB design is needed when you need to assemble your PCB and is in general needed for anyone interested in your PCB.

Video 25-26 [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) gives examples on how to do documentation in Orcad.

### Exercise instructions

1. Create a bill of materials 
2. Create a pdf for the schematic 
3. Create a pdf for your different layers, especially the etch layers are important for component placement
4. Create a .zip file of your gerbers and drill file
5. Include the documentation in your gitlab project
6. Include a link to the documentation in the readme of your gitlab project

