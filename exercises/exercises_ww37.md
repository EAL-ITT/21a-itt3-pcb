---
Week: 37
tags:
- Design rule check
- Netlist
- Board Outline
- Mechanical symbols
---


# Exercises for ww37

## Exercise 1 - Design rule check

### Information 

It is always a good idea to run a Design Rule Check (DRC) before generating a netlist. 
This will remove errors before transferring your design to pcb editor. 
The schematic DRC will check your design for design rule violations and place error markers on your schematic design

### Exercise instructions

1. Watch this video [https://resources.orcad.com/orcad-capture-tutorials/orcad-capture-tutorial-09-perform-a-schematic-drc](https://resources.orcad.com/orcad-capture-tutorials/orcad-capture-tutorial-09-perform-a-schematic-drc)
2. Perform a design rule check on your schematic
3. Fix any errors and repeat from step 2 until all errors are gone
4. Update your worklog!

## Exercise 2 - Netlist

### Information 

To transfer your schematic and footprints to Orcad PCB editor you have to generate a netlist.

### Exercise instructions

1. Watch Video 15 [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) in this playlist to learn how to generate a netlist  
This video from Orcad is showing the netlist generation as well [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-02-generate-a-pcb-editor-netlist-in-orcad-capture](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-02-generate-a-pcb-editor-netlist-in-orcad-capture)
2. Generate a netlist and open it in PCB editor
4. Update your worklog!

## Exercise 3 - Board outline

### Information

Set up design parameters and grid spacing to get ready for your PCB design, draw the board outline and add layers to your design.

### Exercise instructions

1. Watch this video [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline)
2. Create a board outline, do not create more than 2 layers (they do that in the video, you do not want to do this)
4. Update your worklog!

## Exercise 3 - Create mounting holes

### Information

Adding mounting holes to a PCB is a good idea, that way it can be mounted properly in an enclosure.  
Mounting holes are called mechanical symbols that needs to be created in pcb designer as they are not part of the netlist.

### Exercise instructions

1. Watch this video [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols)
2. Create mounting holes on your board
4. Update your worklog!

