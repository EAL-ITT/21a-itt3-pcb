---
title: '21A ITT3 PCB'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: '21A ITT3 PCB, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://eal-itt.gitlab.io/21a-itt3-pcb/21A_ITT3_PCB_weekly_plans.pdf)



