---
Week: 49
tags:
- Exam
- Report
---


# Exercises for ww49

## Exercise 1 - Report

### Information 

The PCB elective is concluded with a report that you hand in on wiseflow.

The report reqirements is described in the [exam description](https://eal-itt.gitlab.io/21a-itt3-pcb/21A_ITT3_PCB_exam_description.pdf) which will also be attached to wiseflow 

It might be a good idea to clean up your gitlab project to make it presentable and useable in terms of linking to it from the report.

### Exercise instructions

1. If you haven't already, read the exam description
2. Create a structure of your report using a table of contents
3. Write the report
4. Proof read the report
5. Correct the report
6. Proof read again
7. Hand in the report