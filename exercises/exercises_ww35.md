---
Week: 35
tags:
- Nordcad beginner course
- Schematic
- Simulation
---


# Exercises for ww35

## Exercise 1 - Nordcad beginners course

### Information 

We will need to be able to draw schematics, simulate circuits and layout PCB's.  
Conveniently Nordcad has created beginner's courses for exactly that.  

### Exercise instructions

1. Register at [https://www.nordcad.eu/login/?action=register&redirect_to=https://www.nordcad.eu/student-forum/](https://www.nordcad.eu/login/?action=register&redirect_to=https://www.nordcad.eu/student-forum/) to gain acces to the Nordcad courses

2. Complete the Nordcad beginner courses located at [https://www.nordcad.eu/student-forum/beginners-course/](https://www.nordcad.eu/student-forum/beginners-course/)  

    The course have 3 sections: 

    * Schematic 
    * Simulation 
    * PCB design

    You need to complete all 3 sections.  

3. While you work, backup each OrCad exercise project to your Gitlab project.  

    *Creating a .gitignore file is a good idea to avoid storing unnessesary files OrCad project files [https://www.toptal.com/developers/gitignore/api/orcad](https://www.toptal.com/developers/gitignore/api/orcad)*

## Exercise 2 - Draw schematic

### Information

Draw a schematic of your chosen Circuit in Orcad capture

### Exercise instructions

1. Make a plan of what sections (sub circuits) you need in your schematic
2. Draw your subcurcuits and connect them using net naming
3. Create pdf file(s) of your schematic and include them in your gitlab project
4. Include the OrCad project in your gitlab project

## Exercise 3 - Simulation

### Information

In order to know if your sub circuits work as intended you need to simulate them.
It might not be possible to simulate all sub circuits but you should be able to simulate power sections, crystal oscillators etc.

The output from the simulations will be important for comparison when testing your assembled PCB in week 48/49 

### Exercise instructions

1. Make an Orcad project for each of the subcircuits you want to test, you want to simulate as many subcircuits as possible
2. Make sure you have selected components with at pspice profile attached. All components in the pspice menu have this.
3. Simulate and probe outputs, voltage levels, currents and power dissapation
4. Document each result in nicely formatted documents for each subcircuit, it needs to be formatted in a way that can be used at the testing stage of the assembled PCB.
5. Include you simulation documents in your gitlab project 

## Comments 

You might find that the Orcad capture turorials is a great help [https://resources.orcad.com/orcad-capture-tutorials](https://resources.orcad.com/orcad-capture-tutorials)