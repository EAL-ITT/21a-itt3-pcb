---
Week: 44
Content:  Factory design constraints, Board layout, Layers
Material: See links in weekly plan
Initials: NISI
---

# Week 44 - Factory design constraints, Board layout, Design layers

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Board and layers setup completed
* Update project log

### Learning goals
* PCB editor board setup
    * Level 1: The student knows how to set up constraints according to JLCPCB design constraints
    * Level 2: The student knows how to set up a board layout in Orcad PCB editor
    * Level 3: The student knows what design layers are and their purpose


## Schedule

*Remember that lectures this week is on Thursday November 4th*

* 08:15 Theory lectures (K1)
* 10:00 Exercises with teacher (K4)
* 12:15 Lunch
* 13:00 Exercises with teacher (K4)
* 14:45 Exercises without teacher (K2/K3)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww44](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww44)

## Comments

* none