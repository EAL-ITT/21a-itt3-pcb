---
Week: 36
Content:  SMT Component packages, footprints, component research
Material: See links in weekly plan
Initials: NISI
---

# Week 36 - SMT Component packages, component research, footprints

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Read and Complete all exercises
* Project log updated

### Learning goals
* SMT component packages (LGK3, LGS1, LGS4, LGC5)
    * Level 1: The student has knowledge about the appereance and sizes of SMT components
    * Level 2: The student knows how different SMT components look in real life
    * Level 3: The student can document findings with appropiate coverage of key parameters 

* Component research (LGK1, LGS1, LGS2, LGS4, LGC2, LGC4)
    * Level 1: The student knows which components that are need in their design
    * Level 2: The student can find and document relevant technical details about each component
    * Level 3: The student knows the total cost of components related to their PCB project

* Footprints (LGK2, LGS1, LGC3, LGC5)
    * Level 1: The student can aquire knowledge on selecting and assigning footprints to a schematic
    * Level 2: The student can aquire footprints from different databases
    * Level 3: The student can create footprint(s)


## Schedule

* 9:00 Exercises without teacher (K2/K3)
* 14:00 Theory lectures (K1)
* 15:00 Exercises with teacher (K4)
* 16:15 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww36](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww36)

## Comments

* none