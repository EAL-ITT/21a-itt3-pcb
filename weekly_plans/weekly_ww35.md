---
Week: 35
Content:  Nordcad beginner course, schematic, simulation 
Material: See links in weekly plan
Initials: NISI
---

# Week 35 - Nordcad beginner course, schematic, simulation 

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Read and Complete all exercises
* Nordcad beginners course completed
* Schematic completed
* Subcircuits simulated and output documented
* Project log updated

### Learning goals

* Nordcad beginners course (LGK3, LGK4, LGK5, LGS3, LGC5)
    * Level 1: The student can draw a schematic using OrCad Capture
    * Level 2: The student can perform pspice simulation in OrCad Capture
    * Level 3: The student knows how to layout a basic PCB  

* Schematic (LGK3, LGS2, LGS4, LGC3)

    * Level 1: The student can divide their circuit into individual sub circuits
    * Level 2: The student can arrange subcircuits and connect them using netnaming 
    * Level 3: The student can document the schematic process

* Simulation (LGK3, LGK5, LGS3, LGC3)
    * Level 1: The student can set up Pspice simulation ofsub circuits
    * Level 2: The student can choose appropiate simulation results
    * Level 3: The student can document simulation results with later testing in mind

## Schedule

* 08:15 Theory lectures (K1)
    * Inside a PCB soldering factory [https://youtu.be/24ehoo6RX8w](https://youtu.be/24ehoo6RX8w)
* 10:00 Exercises with teacher (K4)
* 12:15 Lunch
* 13:00 Exercises with teacher (K4)
* 14:45 Exercises without teacher (K2/K3)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww35](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww35)

## Comments

* none