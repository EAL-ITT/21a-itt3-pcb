---
Week: 34
Content:  Introduction & circuit decision
Material: See links in weekly plan
Initials: NISI
---

# Week 34 - Introduction & circuit decision

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Read and Complete all exercises
* Gitlab project created 
* Circuit chosen
* OrCad suite installed and student license working 
* Project log updated

### Learning goals
* Circuit research (LGK5, LGS2, LGC3, LGC5)
    * Level 1: The student knows details and complexity in the chosen circuit
    * Level 2: The student can validate different circuits and validate them according to complexity and the students skill level 
    * Level 3: The studnet can aquire new knowledge about the circuit that they are going to design a PCB from

* Setup gitlab documentation project + project log (LGK1, LGC2)
    * Level 1: The student can set up a gitlab project correctly
    * Level 2: The student can manage own tasks in the gitlab project
    * Level 3: The student can document their own work and project progress

* OrCad installation and license (LGS1)
    * Level 1: The student can download and install the OrCad suite
    * Level 2: The student can aquire a student license from Nordcad
    * Level 3: The student can activate the license

## Schedule

* 08:15 Introduction (K1)
* 10:00 Exercises with teacher (K4)
* 12:15 Lunch
* 13:00 Exercises with teacher (K4)
* 14:45 Exercises without teacher (K2/K3)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww34](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww34)

## Comments
* Introduction slides [https://eal-itt.gitlab.io/21a-itt3-pcb/21A_ITT3_PCB_introduction_slides.pdf](https://eal-itt.gitlab.io/21a-itt3-pcb/21A_ITT3_PCB_introduction_slides.pdf)